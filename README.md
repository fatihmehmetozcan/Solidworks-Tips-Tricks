Taken from SolidWorks World presentations, Youtube videos and [blogs](https://gitlab.com/fatihmehmetozcan/blog-list). As in summary, only parts that interests me are quoted. Will be updated in time

<br>

Check cited content at SolidWorks World website ( [2017](https://solidworksworld.lanyonevents.com/2017/connect/search.ww), [2018](https://solidworksworld.lanyonevents.com/2018/connect/search.ww) ) with given year and presentation code.



<br>

- [2017 - 4436 - 60 Modeling Tips in 60 Minutes](#2017-4436-60-modeling-tips-in-60-minutes)
- [2017 - 4699 - Using 3D Scan Data Effectively and How to Model Curvy Stuff from It](#2017-4699-using-3d-scan-data-effectively-and-how-to-model-curvy-stuff-from-it)
- [VBA](#vba)
- [2018 - 8333 - Large Assembly Performance 2018 Edition](#2018-8333-large-assembly-performance-2018-edition)
- [2018 - 8334  - Large Drawing Performance 2018 Edition](#2018-8334-large-drawing-performance-2018-edition)
- [2018 - 7551 - All the Small Things SOLIDWORKS Tips and Tricks](#2018-7551-all-the-small-things-solidworks-tips-and-tricks)
- [2018 - 8532 - Coping with Non-SOLIDWORKS Users’ Data](#2018-8532-coping-with-non-solidworks-users-data)
- [2018 - 10274 - Achieving Extreme SOLIDWORKS Performance Modeling Methodology](#2018-10274-achieving-extreme-solidworks-performance-modeling-methodology)
- [2018 - 7591 - Master Modeling Techniques](#2018-7591-master-modeling-techniques)
- [2018 - 11372 - Zen and the Art of SOLIDWORKS Surfacing](#2018-11372-zen-and-the-art-of-solidworks-surfacing)

<br>

### 2017 - 4436 - 60 Modeling Tips in 60 Minutes

![10](img/2017-SWW-4436_10.png)

![17](img/2017-SWW-4436_17.png)

![27](img/2017-SWW-4436_27.png)

![29](img/2017-SWW-4436_29.png)

![31](img/2017-SWW-4436_31.png)

<br>

### 2017 - 4699 - Using 3D Scan Data Effectively and How to Model Curvy Stuff from It

![01.11.06](img/2017-SWW-4699-010.mp4_snapshot_01.11.06.jpg)
    
![01.14.10](img/2017-SWW-4699-010.mp4_snapshot_01.14.10.jpg)
    
![01.15.29](img/2017-SWW-4699-010.mp4_snapshot_01.18.13.jpg)
    
![01.18.13](img/2017-SWW-4699-010.mp4_snapshot_01.11.06.jpg)

<br>

### VBA

![vba_1](img/vba_1.PNG)
    
![vba_2](img/vba_2.PNG)
    
<br>

### 2018 - 8333 - Large Assembly Performance 2018 Edition

![52](img/2018-8333_Jones_52.png)

![54](img/2018-8333_Jones_54.png)

![79](img/2018-8333_Jones_79.png)

![90](img/2018-8333_Jones_90.png)

![103](img/2018-8333_Jones_103.png)

![113](img/2018-8333_Jones_113.png)

![122](img/2018-8333_Jones_122.png)

![123](img/2018-8333_Jones_123.png)

![125](img/2018-8333_Jones_125.png)

![134](img/2018-8333_Jones_134.png)

![138](img/2018-8333_Jones_138.png)

![139](img/2018-8333_Jones_139.png)

![169](img/2018-8333_Jones_169.png)

![187](img/2018-8333_Jones_187.png)

![191](img/2018-8333_Jones_191.png)

![249](img/2018-8333_Jones_249.png)

<br>

### 2018 - 8334 - Large Drawing Performance 2018 Edition

![31](img/2018-8334_Jones_31.png)

![40](img/2018-8334_Jones_40.png)

![51](img/2018-8334_Jones_51.png)

![52](img/2018-8334_Jones_52.png)

![104](img/2018-8334_Jones_104.png)

![106](img/2018-8334_Jones_106.png)

![113](img/2018-8334_Jones_113.png)

![117](img/2018-8334_Jones_117.png)

![118](img/2018-8334_Jones_118.png)

![121](img/2018-8334_Jones_121.png)

![124](img/2018-8334_Jones_124.png)

![125](img/2018-8333_Jones_125.png)

<br>

### 2018 - 7551 - All the Small Things SOLIDWORKS Tips and Tricks

![00.24.04](img/2018-SWW-7551-All-the-Small-Things-SOLIDWORKS-Tips-and-Tricks.mp4_snapshot_00.24.04.jpg)

<br>

### 2018 - 8532 - Coping with Non-SOLIDWORKS Users’ Data

![06.00 ](img/2018-SWW-8532-Coping-with-Non-SOLIDWORKS-Users’-Data.mp4_snapshot__06.00.jpg)

![20.48](img/2018-SWW-8532-Coping-with-Non-SOLIDWORKS-Users’-Data.mp4_snapshot__20.48.jpg)

![35.56](img/2018-SWW-8532-Coping-with-Non-SOLIDWORKS-Users’-Data.mp4_snapshot__35.56.jpg)

![36.41](img/2018-SWW-8532-Coping-with-Non-SOLIDWORKS-Users’-Data.mp4_snapshot__36.41.jpg)

![37.09](img/2018-SWW-8532-Coping-with-Non-SOLIDWORKS-Users’-Data.mp4_snapshot__37.09.jpg)

<br>

### 2018 - 10274 - Achieving Extreme SOLIDWORKS Performance Modeling Methodology

![00.23.52](img/2018-SWW-10274-Achieving-Extreme-SOLIDWORKS-Performance-Modeling-Methodology.mp4_snapshot_00.23.52.jpg)

![01.07.10](img/2018-SWW-10274-Achieving-Extreme-SOLIDWORKS-Performance-Modeling-Methodology.mp4_snapshot_01.07.10.jpg)

![01.07.52](img/2018-SWW-10274-Achieving-Extreme-SOLIDWORKS-Performance-Modeling-Methodology.mp4_snapshot_01.07.52.jpg)

<br>

### 2018 - 7591 - Master - Modeling - Techniques

![09.39](img/2018-SWW-7591.mp4_snapshot_09.39.jpg)

![12.22](img/2018-SWW-7591.mp4_snapshot_12.22.jpg)

![13.21](img/2018-SWW-7591.mp4_snapshot_13.21.jpg)

![16.54](img/2018-SWW-7591.mp4_snapshot_16.54.jpg)

![31.53](img/2018-SWW-7591.mp4_snapshot_31.53.jpg)

![34.29](img/2018-SWW-7591.mp4_snapshot_34.29.jpg)

![34.44](img/2018-SWW-7591.mp4_snapshot_34.44.jpg)

<br>

### 2018 - 11372 - Zen and the Art of SOLIDWORKS Surfacing

![00.01.38](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.01.38.jpg)

![00.06.34](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.06.34.jpg)

![00.06.43](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.06.43.jpg)

![00.06.53](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.06.53.jpg)

![00.07.37](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.07.37.jpg)

![00.07.46](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.07.46.jpg)

![00.10.50](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.10.50.jpg)

![00.32.44](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.32.44.jpg)

![00.34.48](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.34.48.jpg)

![00.35.49](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.35.49.jpg)

![00.44.18](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.44.18.jpg)

![00.58.29](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_00.58.29.jpg)

![01.17.10](img/2018-SWW-11372-Zen-and-the-Art-of-SOLIDWORKS-Surfacing.mp4_snapshot_01.17.10.jpg)